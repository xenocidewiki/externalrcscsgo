#include <windows.h>
#include <iostream>
#include <TlHelp32.h>

DWORD m_dwPID;
HANDLE hProc;

bool getProcHandle(char* ProcName)
{
	HANDLE hPID = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 pProcEntry;

	do
		if (!strcmp((const char*)pProcEntry.szExeFile, ProcName)) {
			m_dwPID = pProcEntry.th32ProcessID;

			CloseHandle(hPID);

			hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_dwPID);
			GetLastError();
			return true;
		}
		else
			Sleep(1);
	while (Process32Next(hPID, &pProcEntry));

	return false;
}

MODULEENTRY32 getModuleInfo(char * ModuleName)
{
	HANDLE hModule = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, m_dwPID);

	MODULEENTRY32 mModEntry;
	mModEntry.dwSize = sizeof(mModEntry);

	do
		if (!strcmp((const char*)mModEntry.szModule, ModuleName)) {
			CloseHandle(hModule);
			return mModEntry;
		}
		else
			Sleep(1);
	while (Module32Next(hModule, &mModEntry));

	mModEntry = { NULL };

	return mModEntry;
}

typedef struct {
	float x, y, z;
}Vector;

int main()
{

	std::cout << "Faggot playing csgo" << std::endl;
	MODULEENTRY32 pClient, pEngine;
	getProcHandle("csgo.exe");
	if (!hProc) {
		std::cout << "get rekt";
	}

	pClient = getModuleInfo("client.dll");
	pEngine = getModuleInfo("engine.dll");

	DWORD pLocalPlayer;
	DWORD ClientState;
	BOOL bEnabled = false;
	INT pShotsFired;
	Vector CurrentViewAngles;
	Vector vPunch;
	Vector NewViewAngles;
	Vector OldAimPunch;
	OldAimPunch.x = OldAimPunch.y = OldAimPunch.z = 0;


	while (hProc) {

		if (GetAsyncKeyState(VK_NUMPAD1) && !bEnabled) { bEnabled = true; }
		if (GetAsyncKeyState(VK_NUMPAD2) && bEnabled) { bEnabled = false; }

		ReadProcessMemory(hProc, (LPVOID)((DWORD)pClient.modBaseAddr + 0xA6D444), &pLocalPlayer, sizeof(DWORD), NULL);
		ReadProcessMemory(hProc, (LPVOID)(pLocalPlayer + 0x4C28), &vPunch, sizeof(Vector), NULL);
		ReadProcessMemory(hProc, (LPVOID)((DWORD)pLocalPlayer + 0xBEB0), &pShotsFired, sizeof(INT), NULL);

		if (pShotsFired > 1 && bEnabled) {

			ReadProcessMemory(hProc, (LPVOID)((DWORD)pEngine.modBaseAddr + 0x6062B4), &ClientState, sizeof(DWORD), NULL);
			ReadProcessMemory(hProc, (LPVOID)((DWORD)ClientState + 0x4D0C), &CurrentViewAngles, sizeof(Vector), NULL);
			NewViewAngles.x = ((CurrentViewAngles.x + OldAimPunch.x) - (vPunch.x * 2.f));
			NewViewAngles.y = ((CurrentViewAngles.y + OldAimPunch.y) - (vPunch.y * 2.f));
			NewViewAngles.z = 0;

			while (NewViewAngles.y > 180)
				NewViewAngles.y -= 360;

			while (NewViewAngles.y < -180)
				NewViewAngles.y += 360;

			if (NewViewAngles.x > 89.0f)
				NewViewAngles.x = 89.0f;

			if (NewViewAngles.x < -89.0f)
				NewViewAngles.x = -89.0f;

			NewViewAngles.z = 0;

			OldAimPunch.x = vPunch.x * 2.f;
			OldAimPunch.y = vPunch.y * 2.f;
			OldAimPunch.z = 0;

			WriteProcessMemory(hProc, (LPVOID)((DWORD)ClientState + 0x4D0C), &NewViewAngles, sizeof(Vector), NULL);
			Sleep(1);
		}
		else { OldAimPunch.x = OldAimPunch.y = OldAimPunch.z = 0; }

	}

	getchar();
	return 0;
}